﻿using System;

namespace CalcInterface
{
    public interface ICalculate
    {
        string DoCalc();
    }
}