﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorManagers;


namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
                Manager manager = new Manager();
                Console.WriteLine(
                    "1: Calculate integer numbers" +
                    "\n2: Calculate float numbers" +
                    "\n3: Calsulate complex numbers");               
            while (true)
            {
                Console.WriteLine("Make your choose:");
                if (!int.TryParse(Console.ReadLine(), out int choose)) Console.WriteLine("Choose only proposed variants");
                else
                {
                    switch (choose)
                    {
                        case 1: Console.WriteLine("Result = {0}",manager.MagicInt()); break;
                        case 2: Console.WriteLine("Result = {0}",manager.MagicDouble()); break;
                        case 3: Console.WriteLine("Result = {0}",manager.Magic_Complex()); break;
                        default: Console.WriteLine("Choose only proposed variants!"); break;
                    }
                }
            }                        
        }
    }
}