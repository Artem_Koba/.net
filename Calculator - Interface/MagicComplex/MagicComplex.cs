﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcInterface;

namespace Magic_Complex
{
    public class MagicComplex : ICalculate
    {
        private string first; public string First { get { return first; } set { first = value; } }
        private string second; public string Second { get { return second; } set { second = value; } }
        private char action; public char Action { get { return action; } set { action = value; } }
        public MagicComplex (string First, string Second, char Action)
        {
            first = First;
            second = Second;
            action = Action;
        }
        public string DoCalc()
        {
            double a = 0;            
            double b = 0;
            char First_i = ' ';
            
            string temp_a = "";
            string temp_b = "";


            for (int i = 0; i < First.Length; ++i)
            {
                if (i == 0 && First[i] == '-') temp_a += First[i];
                else if (char.IsDigit(First[i])&& temp_b == "") temp_a += First[i];
                else if (First[i] == '-' || First[i] == '+') temp_b += First[i];
                else if (char.IsDigit(First[i])) temp_b += First[i];
                else First_i = First[i];
                if (i == First.Length - 1) { a = double.Parse(temp_a); b = double.Parse(temp_b);}
            }

            double c = 0;           
            double d = 0;
            char Second_i = ' ';
            string temp_c = "";
            string temp_d = "";

            for (int i = 0; i < Second.Length; ++i)
            {
                if (i == 0 && Second[i] == '-') temp_c += Second[i];
                else if (char.IsDigit(Second[i]) && temp_d == "") temp_c += Second[i];
                else if (Second[i] == '-' || Second[i] == '+') temp_d += Second[i];
                else if (char.IsDigit(Second[i])) temp_d += Second[i];
                else Second_i = Second[i];
                if (i == Second.Length - 1) { c = double.Parse(temp_c); d = double.Parse(temp_d); }
            }

            switch (Action)
            {
                case '+' :
                    {
                        double result_real;
                        result_real = a + c;
                        double result_imaginary;

                        result_imaginary = b + d;

                        string result_real_st;
                        string result_imaginary_st;

                        if (result_real == 0) result_real_st = "";
                        else result_real_st = Convert.ToString(result_real);
                        if (result_imaginary == 0) { result_imaginary_st = ""; Second_i = ' '; }
                        else if (result_imaginary > 0 && result_real !=0)   result_imaginary_st = Convert.ToString("+" + result_imaginary);
                        else result_imaginary_st = Convert.ToString(result_imaginary);
                        string result = result_real_st + result_imaginary_st + Second_i;
                        return result;
                    }
                    
                case '-':
                    {
                        double result_real;
                        result_real = a - c;
                        double result_imaginary;

                        result_imaginary = b - d;

                        string result_real_st;
                        string result_imaginary_st;

                        if (result_real == 0) result_real_st = "";
                        else result_real_st = Convert.ToString(result_real);
                        if (result_imaginary == 0) { result_imaginary_st = ""; Second_i = ' '; }
                        else if (result_imaginary > 0 && result_real != 0) result_imaginary_st = Convert.ToString("+" + result_imaginary);
                        else result_imaginary_st = Convert.ToString(result_imaginary);
                        string result = result_real_st + result_imaginary_st + Second_i;
                        return result;
                    }
                case '*':
                    {
                        double result_real;
                        result_real = a*c - b*d;
                        double result_imaginary;

                        result_imaginary = a*d+b*c;

                        string result_real_st;
                        string result_imaginary_st;

                        if (result_real == 0) result_real_st = "";
                        else result_real_st = Convert.ToString(result_real);
                        if (result_imaginary == 0) { result_imaginary_st = ""; Second_i = ' '; }
                        else if (result_imaginary > 0 && result_real != 0) result_imaginary_st = Convert.ToString("+" + result_imaginary);
                        else result_imaginary_st = Convert.ToString(result_imaginary);
                        string result = result_real_st + result_imaginary_st + Second_i;
                        return result;
                    }
                default: return "0";
            }            
        }
    }
}
