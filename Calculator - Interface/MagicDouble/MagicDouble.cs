﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcInterface;

namespace Magic_Double
{
    public class MagicDouble : ICalculate
    {
        private string first; public string First { get { return first; } set { first = value; } }
        private string second; public string Second { get { return second; } set { second = value; } }
        private char action; public char Action { get { return action; } set { action = value; } }
        public MagicDouble(string First, string Second, char Action)
        {
            first = First;
            second = Second;
            action = Action;
        }
        public  string DoCalc()
        {
            double first_Double = double.Parse(First);
            double second_Double = double.Parse(Second);
            string result;
            switch (Action)
            {
                case '+': result = Convert.ToString(first_Double + second_Double); return result;
                case '-': result = Convert.ToString(first_Double - second_Double); return result;
                case '*': result = Convert.ToString(first_Double * second_Double); return result;
                case '/': result = Convert.ToString(first_Double / second_Double); return result;
                default: return "0";
            }
        }
    }
}
