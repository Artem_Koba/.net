﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcInterface;

namespace Magic_Int
{
    public class MagicInt : ICalculate
    {
        private string first; public string First { get { return first; } set { first = value; } }
        private string second; public string Second { get { return second; } set { second = value; } }
        private char action; public char Action { get { return action; } set { action = value; } }
        public MagicInt(string First, string Second, char Action)
        {
            first = First;
            second = Second;
            action = Action;
        }
        public string DoCalc()
        {
            int first_int = int.Parse(First);
            int second_int = int.Parse(Second);
            string result;
            switch (Action)
            {
                case '+': result = Convert.ToString(first_int + second_int); return result;
                case '-': result = Convert.ToString(first_int - second_int); return result;
                case '*': result = Convert.ToString(first_int * second_int); return result;
                case '/': result = Convert.ToString(first_int / second_int); return result;
                default: return "0";
            }
        } 
    }
}
