﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magic_Int;
using Magic_Complex;
using Magic_Double;
using Magic;
using Validation;


namespace CalculatorManagers
{
    
    class Manager
    {
        
        Validation_data validation = new Validation_data();
        
        public void MagicInt()
        {
            string first="";
            string second="";
            char action=' ';
            while (first == "")
            {
                Console.WriteLine("Enter first variable: ");
                string first_temp = Console.ReadLine();
                if (validation.validationInt(first_temp)) first = first_temp;
                else Console.WriteLine("You must enter only integer numbers!");
            }
            while (action == ' ')
            {
                Console.WriteLine("Enter action: ");
                if (char.TryParse(Console.ReadLine(), out char action_temp))
                    action_temp = validation.validationAction(action_temp);
                else Console.WriteLine("Enter only 1 symbol!");
                if (action_temp != '!') action = action_temp;     
                else Console.WriteLine("You can choose only +,-,*,/.");
            }
            while (second == "")
            {
                Console.WriteLine("Enter second variable: ");
                string second_temp = Console.ReadLine();
                if (validation.validationInt(second_temp)) second = second_temp;
                else Console.WriteLine("You must enter only integet numbers!");
            }
            MagicInt integer = new MagicInt(first, second, action);            
            integer.DoCalc();
            
        }
        public void Magic_Complex()
        {            
            string first = "";
            string second = "";
            char action = ' ';
            while (first == "")
            {
                Console.WriteLine("Enter first variable: ");
                string first_temp = Console.ReadLine();
                if (validation.validationComplex(first_temp)) first = first_temp;
                else Console.WriteLine("You must enter only like in example (a+b!)!");
            }
            while (action == ' ')
            {
                Console.WriteLine("Enter action: ");
                if (char.TryParse(Console.ReadLine(), out char action_temp) && action_temp != '!' && action_temp != '/')
                {
                    action_temp = validation.validationAction(action_temp);
                    action = action_temp;
                }                           
                else Console.WriteLine("Enter only 1 symbol and you can choose only +,-,*.");
            }
            while (second == "")
            {
                Console.WriteLine("Enter second variable: ");
                string second_temp = Console.ReadLine();
                if (validation.validationComplex(second_temp)) second = second_temp;
                else Console.WriteLine("You must enter only like in example(a + b!)!");
            }

            MagicComplex Complex = new MagicComplex(first,second,action);            
            Complex.DoCalc();
        }
        public void MagicDouble()
        {
            string first = "";
            string second = "";
            char action = ' ';
            while (first == "")
            {
                Console.WriteLine("Enter first variable: ");
                string first_temp = Console.ReadLine();
                if (validation.validationDouble(first_temp)) first = first_temp;
                else Console.WriteLine("You must enter only float numbers!");
            }
            while (action == ' ')
            {
                Console.WriteLine("Enter action: ");
                if (char.TryParse(Console.ReadLine(), out char action_temp))
                    action_temp = validation.validationAction(action_temp);
                else Console.WriteLine("Enter only 1 symbol!");
                if (action_temp != '!') action = action_temp;
                else Console.WriteLine("You can choose only +,-,*,/.");
            }
            while (second == "")
            {
                Console.WriteLine("Enter second variable: ");
                string second_temp = Console.ReadLine();
                if (validation.validationDouble(second_temp)) second = second_temp;
                else Console.WriteLine("You must enter only integet numbers!");
            }
            MagicDouble FloatNumber = new MagicDouble(first, second, action);            
            FloatNumber.DoCalc();
        }              
    }
}