﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic
{
    public abstract class CalcMagic
    {
        
        private string first; public string First { get { return first; } set { first = value; } }
        private string second; public string Second { get { return second; } set { second = value; } }
        private char action; public char Action { get { return action; } set { action = value; } }
        
        public void ShowMessage(string message)
        {            
            Console.WriteLine($"Result = {message}");
        }
        public CalcMagic(string first, string second,char action)
        {
            this.First = first;
            this.Second = second;
            this.Action = action;
        }

        public virtual void DoCalc() {  }
    }
}
