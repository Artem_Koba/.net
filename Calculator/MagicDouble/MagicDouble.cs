﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magic;

namespace Magic_Double
{
    public class MagicDouble : CalcMagic
    {
        public delegate void CalculatonDone(string message);
        public event CalculatonDone CalcCompleted;
        public MagicDouble(string First, string Second, char Action) : base(First, Second, Action) { }
        public override void DoCalc()
        {
            double first_Double = double.Parse(First);
            double second_Double = double.Parse(Second);
            string result;
            switch (Action)
            {

                case '+': result = (first_Double + second_Double).ToString(); break;
                case '-': result = (first_Double - second_Double).ToString(); break;
                case '*': result = (first_Double * second_Double).ToString(); break;
                case '/': result = (first_Double / second_Double).ToString(); break;
                default:result = "Choose only proposed variants"; break;                    
            }
            CalcCompleted += ShowMessage;
            CalcCompleted(result);
        }
    }
}
