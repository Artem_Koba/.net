﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magic;


namespace Magic_Int
{
    
    public class MagicInt : CalcMagic
    {
        public delegate void CalculatonDone(string message);
        public event CalculatonDone CalcCompleted;
        public MagicInt(string First, string Second, char Action) : base(First, Second, Action) { }       
        
        public override void DoCalc()
        {
            
            int first_int = int.Parse(First);
            int second_int = int.Parse(Second);
            string result;
            switch (Action)
            {
                case '+': result = (first_int + second_int).ToString();break;
                case '-': result = (first_int - second_int).ToString(); break;
                case '*': result = (first_int * second_int).ToString(); break;
                case '/': result = (first_int / second_int).ToString(); break;
                default: result = "Choose only proposed variants";break;
            }
            CalcCompleted += ShowMessage;
            CalcCompleted(result);           
        }
        

             
    }
}
