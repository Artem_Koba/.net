﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation
{
    public class Validation_data
    {
        public bool validationInt(string text)
        {
            for(int i = 0;i<text.Length;++i)
            {
                if (!char.IsDigit(text[i])) return false;
            }
            return true;
        }
        public bool validationDouble(string text)
        {
            for (int i = 0; i < text.Length; ++i)
            {
                if (char.IsDigit(text[i]) || text[i] == ',') { }
                else return false;
            }
            return true;
        }
        public bool validationComplex(string text)
        {
            bool first = false;
            bool action = false;
            bool second = false;
            bool index = false;
            for (int i = 0; (first != true || action != true || second !=true || index !=true); ++i)
            {
                if (i >= text.Length || text.Length==0) return false;
                else if (i == 0 && text[i] == '-') { }
                else if (char.IsDigit(text[i]) && first != true) { }
                else if (text[i] == '+' || text[i] == '-' && action != true) { first = true; action = true; }
                else if (char.IsDigit(text[i])) { }
                else if (text[i] == 'i') { second = true; index = true; }
                else return false;               
            }
            return true;
        }
        public char validationAction(char action)
        {
            switch (action)
            {
                case '+': return '+';
                case '-': return '-';
                case '*': return '*';
                case '/': return '/';
                default: return '!';
            }
        }
    }
}