﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLogic
{
    public class Logic
    {
        public double magic(double first, double second, char symb)
        {
            switch (symb)
            {
                case '-': return first - second; break;
                case '+': return first + second; break;
                case '*': return first * second; break;
                case '/': if (second != 0) return first / second; else { Console.WriteLine("Error!!!"); return 0; }; break;
                default: return 0;
            }

        }
    }
}
