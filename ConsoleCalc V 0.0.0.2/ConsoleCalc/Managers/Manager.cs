﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcLogic;

namespace ConsoleCalc2
{

    class Manager
    {
        public double result = 0;
        Logic Calculator = new Logic();
        public void inputData()
        {
            try
            {
                Console.WriteLine("Enter your first value:");
                double first = Convert.ToDouble(Console.ReadLine()); 
                Console.WriteLine("Enter your second value:");
                double second = Convert.ToDouble(Console.ReadLine()); 
                Console.WriteLine("Choose your action: +, -, *,/.");
                char symb = Convert.ToChar(Console.ReadLine());
                this.result = Calculator.magic(first, second, symb);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public void OutputData()
        {
            if (result == 0);
            else
                Console.WriteLine("Your result =" + result);
        }
    }
}
