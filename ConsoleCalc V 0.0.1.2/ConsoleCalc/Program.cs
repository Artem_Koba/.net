﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalc2
{
    
    class Program
    {
        

        static void Main(string[] args)
        {
            Manager manager = new Manager();
            Console.WriteLine("Enter 'q' for exit program");
            while (true)
            {
                try
                {

                    if (manager.quit == 'q') if (manager.check()) break; else { manager.quit = ' '; continue; }
                    Console.WriteLine("Input first variable:");
                    manager.inputFirst();
                    if (manager.quit == 'q') if (manager.check()) break; else { manager.quit = ' '; continue; }
                    Console.WriteLine("Input second variable:");
                    manager.inputSecond();
                    if (manager.quit == 'q') if (manager.check()) break; else { manager.quit = ' '; continue; }
                    Console.WriteLine("Input action:");
                    manager.inputAction();
                    if (manager.quit == 'q') if (manager.check()) break; else { manager.quit = ' '; continue; }
                    Console.WriteLine("Your result = " + manager.OutputData());
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Dont left this field empty!");
                }
            }
        }
    }
}
