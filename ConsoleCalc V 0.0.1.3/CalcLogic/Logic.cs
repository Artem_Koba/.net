﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLogic
{
    public class Logic
    {
        public double magic(double first, double second, char symb)
        {
            
                switch (symb)
                {
                    case '-': return first - second; 
                    case '+': return first + second; 
                    case '*': return first * second; 
                    case '/': if (second != 0) return first / second; else throw new FormatException();
                    default: { return 0; throw new FormatException();};
                }                      

        }
    }
}
