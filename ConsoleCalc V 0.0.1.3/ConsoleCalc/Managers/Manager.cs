﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcLogic;

namespace ConsoleCalc2
{

    class Manager
    {
        public double result = 0;
        public char quit = ' ';
        public double first = 0;
        public double second = 0;
        public char action = ' ';
        private readonly Logic Calculator = new Logic();
        public void inputFirst()
        {
                string temp = Console.ReadLine();
                switch (temp[0])
            {
                    case 'q': quit = 'q'; break;
                    default: if (!double.TryParse(temp, out first)) throw new FormatException(); break;
                }
        }
        public bool check()                                                                                                     // Поддтверждение выхода
        {
            Console.WriteLine("Are you shure you want exit program? y/n ");                 
            char acept = Convert.ToChar(Console.ReadLine());
            if (acept == 'y')
                return true;
            else if (acept == 'n') return false;
            else throw new FormatException();
        }
        public void inputSecond()
        {
                string temp = Console.ReadLine();
                switch (temp[0])
                {
                    case 'q': quit = 'q'; break;
                    default: if (!double.TryParse(temp, out second)) throw new FormatException(); break;
                }            
        }
        public void inputAction()
        {
            
            if (!char.TryParse(Console.ReadLine(), out action)) throw new FormatException();
            if (action == 'q') quit = 'q';           
        }

        public double OutputData()
        {
            return Calculator.magic(first,second,action);
        }
        public void check(char q)
        {

        }
    }    
}
