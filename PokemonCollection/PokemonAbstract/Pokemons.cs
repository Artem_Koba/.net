﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;


namespace Pokemons
{
    [XmlInclude(typeof(WaterPoke))]
    [XmlInclude(typeof(EarthPoke))]
    [Serializable]   
    public abstract class Pokemon
    {
        
        private DateTime creation = DateTime.Now;
        public DateTime Creation { get { return creation; } set { creation = value; } }
        private string name;
        public string Name { get { return name; } set { name = value; } }
        private double height;
        public double Height { get { return height; } set { height = value; } }
        private double weight;
        public double Weight { get { return weight; } set { weight = value; } }
        public virtual void ShowInfo() { }
        public Pokemon() { }
        public Pokemon(string name, double height, double weight)
        {
            Name = name;
            Height = height;
            Weight = weight;
        }
        
    }
   [Serializable]
    public class WaterPoke : Pokemon
    {        
        public WaterPoke() { }
        public WaterPoke(string name, double height, double weight, double movement) : base(name, height, weight)
        {
            Swiming_speed = movement;
        }

        private string type = "Water"; public string Type { get { return type; } set { type = value; } }
        private double swiming_speed; public double Swiming_speed { get { return swiming_speed; } set { swiming_speed = value; } }
        public override string ToString()
        {
            return "1. Name:" + Name + "\n2. Weight: " + Weight + "\n3. Height: " + Height + "\n4. Swiming speed: " + swiming_speed + "\n5. Type: " + Type + "\n6. Date of creation: " + Creation;
        }
    }
    [Serializable]
    public class EarthPoke : Pokemon
    {
        public EarthPoke() { }
        public EarthPoke(string name, double height, double weight, double movement) : base(name, height, weight)
        {
            Instillation_speed = movement;
        }

        private string type = "Earth"; public string Type { get { return type; } set { type = value; } }
        private double instillation_speed; public double Instillation_speed { get { return instillation_speed; } set { instillation_speed = value; } }
        public override string ToString()
        {
            return "1. Name:" + Name + "\n2. Weight: " + Weight + "\n3. Height: " + Height + "\n4. Instillation speed: " + Instillation_speed + "\n5. Type: " + Type + "\n6. Date of creation: " + Creation;
        }
    }
}
