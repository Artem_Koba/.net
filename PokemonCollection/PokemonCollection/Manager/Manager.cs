﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poke.List;
using System.Collections;
using Validation_data;

namespace PokemonCollection.manager
{
    class PokeManager
    {
        delegate void DelegetaForEvent(string message);
        event DelegetaForEvent BeforeAction;
        event DelegetaForEvent AfterAction;

        private Validation test = new Validation();
        private PokeList List = new PokeList();
        private List<string> elements = new List<string>() { "Earth", "Water" };                                                      //Массив для типов покемонов, если добавляется новый класс, тип вписать сюда
        private List<string> movements = new List<string>() { "Instillation speed", "Swiming speed" };                              //Массив для уникальных способностей покемонов
        public void AddNewPoke(int index)                                                                                   //Переменая index необходима для указания на место какого покемона добавлять замену.
        {
            BeforeAction += ShowMessageBefore;                                                                              //Если index == 0, тогда добавляется покемон в начало коллекции
            AfterAction += ShowMessageAfter;
            string name;
            double height;
            double weight;
            string type;
            double movement;
            BeforeAction("Creation of poke started");
            while (true)
            {
                Console.WriteLine("Name poke: ");
                string name_temp = Console.ReadLine();
                if (test.ValidationText(name_temp)) { name = name_temp; break; }
                else Console.WriteLine("Poke name must have only letters and '_' ! " +
                    "Or you need enter at least 1 symbol");
            }           //name
            while (true)
            {
                Console.WriteLine("Height poke: ");
                if (double.TryParse(Console.ReadLine(), out height)) break;
                else Console.WriteLine("Enter only integer or float numbers!");
            }           //height    
            while (true)
            {
                Console.WriteLine("Weight poke: ");
                if (double.TryParse(Console.ReadLine(), out weight)) break;
                else Console.WriteLine("Enter only integer or float numbers!");
            }           //weight   
            int type_temp;
            while (true)
            {

                Console.WriteLine("Type poke: ");
                for (int i = 0; i < elements.Count; ++i)
                    Console.WriteLine("{0}: {1}", i, elements[i]);                                                      //Инфа берется из массива типов для масштабности
                if (int.TryParse(Console.ReadLine(), out type_temp))
                    if (type_temp >= 0 && type_temp < elements.Count) { type = elements[type_temp]; break; }
                    else { }
                else Console.WriteLine("Choose only proposed variants!");
            }
            while (true)
            {
                Console.WriteLine("{0}:", movements[type_temp]);
                if (double.TryParse(Console.ReadLine(), out double movement_temp)) { movement = movement_temp; break; }  //Инфа берется из массива способностей для масштабности
                else Console.WriteLine("Poke movement can have only integer or float nubmers!");
            }
            List.Add(name, height, weight, type, index, movement);
            AfterAction("Poke was created.");
        }
        public void DeletePoke()
        {
            BeforeAction += ShowMessageBefore;                                                                             
            AfterAction += ShowMessageAfter;
            while (true)
            {
                BeforeAction("Input index poke, what you want delete:");
                ShowAllPokeList();
                if (int.TryParse(Console.ReadLine(), out int index) && index < List.CountPokelist() && index >= 0) { List.DeletePoke(index); break; }    //Проверка индекса по наличию его в коллекции
                else Console.WriteLine("There is no poke index in collection!");
            }
            ShowMessageAfter("Poke was deleted.");
        }
        public void ChangePoke()
        {                                                                                                                 //Для замены типа покемона необходимо его пересоздать с новым типом, изачально сохраняются 
            int index;                                                                                                    //неизменяемые переменные, затем удаляется старый покемон и добавляется новый с сохранненными полями
            while (true)
            {
                ShowAllPokeList();
                Console.WriteLine("Enter index poke, what you want to change: ");
                if (!int.TryParse(Console.ReadLine(), out index) && index >= List.PokeNumbers()) Console.WriteLine("Index must only integer numbers and be less, then poke numbers in collection");
                else break;
            }
            List.ShowPoke(index);
            Console.WriteLine("You want change all poke info? y/n");
            while (true)
            {
                char choose_poke = char.Parse(Console.ReadLine());
                if (choose_poke == 'y') { List.DeletePoke(index); AddNewPoke(index); }
                else if (choose_poke == 'n')
                {
                    Console.WriteLine("What characteristic you want to change?"); char choose_character = char.Parse(Console.ReadLine());
                    dynamic Poke = List.TakePoke(index);
                    switch (choose_character)
                    {
                        case '1':
                            while (true)
                            {
                                Console.WriteLine("Enter new name: ");
                                string name = Console.ReadLine();
                                if (test.ValidationText(name)) { List.ChangePokeName(index, name); break; }
                                else Console.WriteLine("Name can only contain letters and spase!");
                            }
                            break;
                        case '2':
                            while (true)
                            {
                                Console.WriteLine("Enter new height: ");
                                if (double.TryParse(Console.ReadLine(), out double height)) { List.ChangePokeHeight(index, height); break; }
                                else Console.WriteLine("Height can only contain integer or double values!");
                            }
                            break;
                        case '3':
                            while (true)
                            {
                                Console.WriteLine("Enter new weight: ");
                                if (double.TryParse(Console.ReadLine(), out double weight)) { List.ChangePokeWeight(index, weight); break; }
                                else Console.WriteLine("Weight can only contain integer or double values!");
                            }
                            break;
                        case '5':
                            int type_index;
                            string type = "";
                            while (true)
                            {
                                Console.WriteLine("Choose new type: ");
                                for (int i = 0; i < elements.Count; ++i)
                                    Console.WriteLine("{0}: {1}", i, elements[i]);                    //Массив elements необходим для масшабирования вывода информации
                                if (int.TryParse(Console.ReadLine(), out type_index))
                                    if (type_index >= 0 && type_index <= elements.Count) { type = elements[type_index]; break; }
                            }
                            Console.WriteLine("New {0}", movements[type_index]);

                            while (true)
                            {
                                if (!double.TryParse(Console.ReadLine(), out double speed_movement))
                                    Console.WriteLine("{0} must be only integer or float number!", movements[type_index]);
                                else List.ChangePokeType(index, type, speed_movement); break;                                        //Индекс необходим для указания, на место какого покемона вставлять нового
                            }
                            break;
                    }
                    break;
                }
                else Console.WriteLine("You need choose yes or no! y/n");
            }


        }
        public void ShowPoke()
        {
            Console.WriteLine("Enter index poke, what you want to see: ");
            List.ShowAllPokelist();
            int index = int.Parse(Console.ReadLine());
            List.ShowPoke(index);
        }
        public void SortPoke()
        {
            char index = ' ';
            while (true)
            {
                Console.WriteLine("You can sort poke by: \n1: type\n2: date of creation");
                if (char.TryParse(Console.ReadLine(), out index))
                    if (index != '1' && index != '2') Console.WriteLine("Enter 1 or 2!");
                break;
            }
            switch (index)
            {
                case '1': List.SortByType(); Console.WriteLine("Collection was sorted"); break;
                case '2':
                    while (true)
                    {
                        Console.WriteLine("How you want to sort collection? " +
                            "Earliest is last or first? " +
                            "\n1: last\n2: first ");
                        char choose = char.Parse(Console.ReadLine());
                        if (choose == '1') { List.SortByDate('1'); Console.WriteLine("Collection was sorted"); break; }
                        else if (choose == '2') { List.SortByDate('2'); Console.WriteLine("Collection was sorted"); break; }
                        else Console.WriteLine("Choose only 1 or 2!");
                    }
                    break;
            }

        }
        public void ShowAllPokeList()
        {
            List.ShowAllPokelist();
        }
        public void Begin(int i)
        {
            List.DeSerialize(i);
        }
        public void End(int i)
        {
            List.Serialize(i);
        }
        public void Clear()
        {
            List.ClearBase();
        }
        public void ShowMessageBefore(string message)
        {
            Console.WriteLine($"Before message = {message}");
        }
        public void ShowMessageAfter(string message)
        {
            Console.WriteLine($"After message = {message}");
        }
    }
}
