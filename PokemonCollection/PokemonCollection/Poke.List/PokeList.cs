﻿using System;
using System.Collections.Generic;
using Pokemons;
using Binary_Serialization;
using XML_Serialization;
using System.IO;

namespace Poke.List
{
    public class PokeList
    {

        public List<Pokemon> pokelist;
        public void Add(string name, double height, double weight, string type, int index, double movement)
        {

            switch (type)
            {
                case "Water":
                    WaterPoke water = new WaterPoke(name, height, weight, movement);
                    if (index == 0)
                        pokelist.Add(water);
                    else pokelist.Insert(index, water);
                    break;
                case "Earth":
                    EarthPoke earth = new EarthPoke(name, height, weight, movement);
                    if (index == 0)
                        pokelist.Add(earth);
                    else pokelist.Insert(index, earth);
                    break;
            }
        }
        public void ShowAllPokelist()
        {
            if (pokelist.Count == 0) Console.WriteLine("There is no poke in collection!");
            for (int i = 0; i < pokelist.Count; ++i)
            {

                if (pokelist[i] is WaterPoke)
                {
                    WaterPoke x = pokelist[i] as WaterPoke;
                    Console.WriteLine("{0} Poke: {1} Type: {2}", i, pokelist[i].Name, x.Type);
                }
                else if (pokelist[i] is EarthPoke)
                {
                    EarthPoke x = pokelist[i] as EarthPoke;
                    Console.WriteLine("{0} Poke: {1} Type: {2}", i, pokelist[i].Name, x.Type);
                }
            }
        }
        public void DeletePoke(int x)
        {
            pokelist.RemoveAt(x);
        }
        public void ChangePokeName(int index, string name)
        {
            pokelist[index].Name = name;
        }
        public void ChangePokeHeight(int index, double height)
        {
            pokelist[index].Height = height;
        }
        public void ChangePokeWeight(int index, double weight)
        {
            pokelist[index].Weight = weight;
        }
        public void ChangePokeType(int index, string type, double movement)
        {
            string pokeName = pokelist[index].Name;
            double height = pokelist[index].Height;
            double weight = pokelist[index].Weight;
            DeletePoke(index);
            Add(pokeName, height, weight, type, index, movement);
        }
        public void ShowPoke(int index)
        {
            pokelist[index].ToString();
        }
        public int PokeNumbers()
        {
            return pokelist.Capacity;
        }
        public dynamic TakePoke(int index)
        {
            return pokelist[index];
        }
        public int CountPokelist()
        {
            return pokelist.Count;
        }
        public void SortByType()
        {
            pokelist.Sort((first, second) => string.Compare(first.GetType().ToString(), second.GetType().ToString()));
        }
        public void SortByDate(char choose)
        {
            switch (choose)
            {
                case '1': pokelist.Sort((first, second) => DateTime.Compare(first.Creation, second.Creation)); break;
                case '2': pokelist.Sort((first, second) => DateTime.Compare(second.Creation, first.Creation)); break;

            }
        }
        public void DeSerialize(int i)
        {
            if (i == 1)
            {
                BinarySerialization binary_serializator = new BinarySerialization();
                pokelist = binary_serializator.DeSerialize();
            }             
            else if(i == 2)
            {
                XMLSerialisation xml_serializator = new XMLSerialisation();
                pokelist = xml_serializator.DeSerialize();
            }
                
        }
        public void Serialize(int i)
        {
            if (i == 1)
            {
                BinarySerialization binary_serializator = new BinarySerialization();
                binary_serializator.Serialize(pokelist);
            }
            else if (i == 2)
            {
                XMLSerialisation xml_serializator = new XMLSerialisation();
                 xml_serializator.Serialize(pokelist);
            }
        }
        public void ClearBase()
        {
            using (FileStream ps = new FileStream("pokemons.xml", FileMode.Create))
            {
                pokelist.Clear();
            }
        }
    }
}
