﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonCollection.manager;
using System.Collections;


namespace PokemonCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            PokeManager manager = new PokeManager();
            int serialisation_type;
            while (true)
            {
                Console.WriteLine("What kind of serialization you want to use?\n" + 
                    "1: Binary\n"+
                    "2: XML\n");
                if (int.TryParse(Console.ReadLine(), out serialisation_type)&& serialisation_type < 3)                     
                    break;
                else Console.WriteLine("You must enter only proposed variants!");
            }
            manager.Begin(serialisation_type);
            bool exit = true;
            while (exit)
            {
                Console.WriteLine("Choose, what you want to do: \n" +
                    "Add new poke:            1\n" +
                    "Delete poke:             2\n" +
                    "Change poke:             3\n" +
                    "Show poke:               4\n" +
                    "Show all poke list:      5\n" +
                    "Sort collection by:      6\n" +
                    "Clear collection:        7\n" +
                    "Exit program:            8\n");
                if (!char.TryParse(Console.ReadLine(), out char choose)) { Console.WriteLine("Need enter minimum one symbol!\n"); continue; }

                switch (choose)
                {
                    case '1': manager.AddNewPoke(0); break;
                    case '2': manager.DeletePoke(); break;
                    case '3': manager.ChangePoke(); break;
                    case '4': manager.ShowPoke(); break;
                    case '5': manager.ShowAllPokeList(); break;
                    case '6': manager.SortPoke(); break;
                    case '7': manager.Clear();Console.WriteLine("Collection cleared!"); break;
                    case '8': manager.End(serialisation_type);exit = false; break;
                    default: Console.WriteLine("Error. You choose wrong variant."); break;
                }
            }
        }
    }
}
