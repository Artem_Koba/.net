﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using Pokemons;

namespace Binary_Serialization
{
    public class BinarySerialization
    {
        BinaryFormatter formatter = new BinaryFormatter();
        public void Serialize(List<Pokemon> pokelist)
        {
            using (FileStream ps = new FileStream("pokemons.dat", FileMode.Create))
            {
                formatter.Serialize(ps, pokelist);
            }
           
        }
        public List<Pokemon> DeSerialize()
        {
            using (FileStream ps = new FileStream("pokemons.dat", FileMode.OpenOrCreate))
            {
                if (ps.Length!=0)
                return (List<Pokemon>)formatter.Deserialize(ps);
                return new List<Pokemon>();
            }
        }      
    }
}
