﻿using System;

namespace Validation_data
{
    public class Validation
    {
        public bool ValidationText(string text)
        {
            if (text.Length == 0) return false;
            for (int i = 0; i < text.Length; i++)
                if (!(char.IsLetter(text[i]) || text[i] == '_')) return false;
            return true;
        }
    }
}
