﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Pokemons;
using System.IO;
namespace XML_Serialization
{
   
    public class XMLSerialisation
    {      
        public void Serialize(List<Pokemon> pokelist)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Pokemon>));
            using (FileStream ps = new FileStream("pokemons.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(ps, pokelist);
            }
            

        }
        public List<Pokemon> DeSerialize()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Pokemon>));
            using (FileStream ps = new FileStream("pokemons.xml", FileMode.OpenOrCreate))
            {
                if (ps.Length != 0)
                    return (List<Pokemon>)formatter.Deserialize(ps);
                return new List<Pokemon>();
            }
        }
    }
}
