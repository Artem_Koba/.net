﻿using ShitChatDAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShitChatDAL
{
    class ChatContext : DbContext
    {
        public ChatContext() : base("DBConnection") { }

        public DbSet<User> Users { get; set; }
        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<UserHistory> UserHistory { get; set; }
        public DbSet<Messages> Messages { get; set; }
        public DbSet<Topic> Themes { get; set; }
    }    
}