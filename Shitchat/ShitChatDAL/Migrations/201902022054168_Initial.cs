namespace ShitChatDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        User = c.String(),
                        Oponent = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Lvl = c.Int(nullable: false),
                        Rank = c.String(),
                        RankImage = c.String(),
                        FavoriteTopic_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Topics", t => t.FavoriteTopic_Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.FavoriteTopic_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Nick = c.String(nullable: false, maxLength: 30),
                        Avatar = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Result = c.Boolean(nullable: false),
                        CompletedDate = c.DateTime(nullable: false),
                        Messages_Id = c.Guid(),
                        Oponent_Id = c.Guid(),
                        Topic_Id = c.Guid(),
                        User_Id = c.Guid(),
                        User_Id1 = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Messages", t => t.Messages_Id)
                .ForeignKey("dbo.Users", t => t.Oponent_Id)
                .ForeignKey("dbo.Topics", t => t.Topic_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.Users", t => t.User_Id1)
                .Index(t => t.Messages_Id)
                .Index(t => t.Oponent_Id)
                .Index(t => t.Topic_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDetails", "Id", "dbo.Users");
            DropForeignKey("dbo.UserHistories", "User_Id1", "dbo.Users");
            DropForeignKey("dbo.UserHistories", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserHistories", "Topic_Id", "dbo.Topics");
            DropForeignKey("dbo.UserHistories", "Oponent_Id", "dbo.Users");
            DropForeignKey("dbo.UserHistories", "Messages_Id", "dbo.Messages");
            DropForeignKey("dbo.UserDetails", "FavoriteTopic_Id", "dbo.Topics");
            DropIndex("dbo.UserHistories", new[] { "User_Id1" });
            DropIndex("dbo.UserHistories", new[] { "User_Id" });
            DropIndex("dbo.UserHistories", new[] { "Topic_Id" });
            DropIndex("dbo.UserHistories", new[] { "Oponent_Id" });
            DropIndex("dbo.UserHistories", new[] { "Messages_Id" });
            DropIndex("dbo.UserDetails", new[] { "FavoriteTopic_Id" });
            DropIndex("dbo.UserDetails", new[] { "Id" });
            DropTable("dbo.UserHistories");
            DropTable("dbo.Users");
            DropTable("dbo.UserDetails");
            DropTable("dbo.Topics");
            DropTable("dbo.Messages");
        }
    }
}
