﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShitChatDAL.Model
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Nick { get; set; }
        
        public string Avatar { get; set; }
        
        public UserDetails Details { get; set; }

        public virtual ICollection<UserHistory> History { get; set; }
    }    
}
