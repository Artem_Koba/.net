﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShitChatDAL.Model
{
    public class UserDetails
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public int Lvl { get; set; }
        public string Rank { get; set; }            
        public string RankImage { get; set; }

        public Topic FavoriteTopic { get; set; } 
        [Required]
        public User User { get; set; }
    }
}