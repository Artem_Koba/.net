﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShitChatDAL.Model
{
    public class UserHistory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id{ get; set; }
        public int Number { get; set; }       
        public bool Result { get; set; }
        public DateTime CompletedDate { get; set; }

        public User User { get; set; } 
        public User Oponent { get; set; }        
        public Topic Topic { get; set; }
        public Messages Messages { get; set; }        
    }
}
